# My Kingdom Death: Monster Digital Asset List #

This isn't code. It's just a directory listing of all my scanned in digital assets for Kingdom  Death: Monster (KD:M).

If I have scanned it, it will be in this list.

I use PowerShell to get this list. The PowerShell snippet used is the following, just need to be in the parent directory:

```powershell
Get-ChildItem -LiteralPath ("\\?\{0}" -f (pwd).Path) -Recurse -File | %{Write-Output ((".{0}\{1}" -f $_.Directory,$_.Name).Replace(("\\?\{0}" -f (pwd).Path),""))} | clip
```

This list will also contain 3rd Party assets that I have collected which can be used with KD:M.
These assets mostly come from Content Creation Group (CCG) and the Community Edition. 

Links to 3rd Party content are the following:

Content Creation Group  - https://www.patreon.com/ccgteam
Community Edition (found on Lantern's Reign Discord) - https://discord.gg/mTAUdC6